package com.moravia.symfonie;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.Lists;
import com.moravia.symfonie.domain.*;
import com.moravia.symfonie.query.Query;
import com.moravia.symfonie.util.json.JsonUtils;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.List;

import static com.moravia.symfonie.domain.BaseAttachment.Type;
import static com.moravia.symfonie.filter.Filters.contains;
import static com.moravia.symfonie.filter.Filters.*;
import static com.moravia.symfonie.filter.Filters.startsWith;
import static com.moravia.symfonie.query.QueryBuilder.emptyQuery;
import static com.moravia.symfonie.query.QueryBuilder.queryBuilder;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class SymfonieImplTest {

    private static final long NON_EXISTENT_ID = 999_999_999l;
    private static final long PROJECT_ID = 27l;
    private static final long REQUESTOR_ID = 12796l;

    private SymfonieImpl symfonie;
    private ObjectMapper jsonMapper;

    private User me;


    @Before
    public void setUp() throws Exception {
        symfonie = new SymfonieImpl();

//        symfonie.setApiUrl("https://test-projects.moravia.com/api");
//        symfonie.setApiToken("27550e37-182d-49e3-aa6e-602da21e9430");

        symfonie.setApiUrl("https://dev-projects.moravia.com/api");
        symfonie.setApiToken("54313749-2d11-4e2b-b629-589e92de57db"); // Marek: this token works on all servers (DEV, TEST, LIVE)
        symfonie.init();

        jsonMapper = JsonUtils.symfonieMapper();
        jsonMapper.enable(SerializationFeature.INDENT_OUTPUT);

        me = symfonie.getMe();
    }

    @After
    public void tearDown() throws Exception {
        symfonie.destroy();
    }


    @Test
    public void testGetUser() throws Exception {
        User user = symfonie.getUser(REQUESTOR_ID);
        assertThat(user.getId(), equalTo(REQUESTOR_ID));
        assertThat(user.getVacations(), notNullValue()); // expanded value
        assertThat(user.getTimeZone(), notNullValue()); // custom convertor in place

        assertThat(symfonie.getUser(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindUsers() throws Exception {
        Query query = queryBuilder().filter(startsWith("Name", "a")).build();
        assertThat(symfonie.findUsers(query), not(emptyCollectionOf(User.class)));
    }

    @Test
    public void testGetMe() throws Exception {
        assertThat(symfonie.getMe(), notNullValue());
    }


    @Test
    public void testGetLanguage() throws Exception {
        assertThat(symfonie.getLanguage(1l), notNullValue());
        assertThat(symfonie.getLanguage(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindLanguages() throws Exception {
        assertThat(symfonie.findLanguages(emptyQuery()), not(emptyCollectionOf(Language.class)));
        assertThat(symfonie.findLanguages(queryBuilder().filter(contains("Name", "cze")).build()), not(emptyCollectionOf(Language.class)));
        assertThat(symfonie.findLanguages(queryBuilder().filter(eq("Id", NON_EXISTENT_ID)).build()), emptyCollectionOf(Language.class));
    }


    @Test
    public void testGetTag() throws Exception {
        assertThat(symfonie.getTag(1l), notNullValue());
        assertThat(symfonie.getTag(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindTags() throws Exception {
        assertThat(symfonie.findTags(emptyQuery()), not(emptyCollectionOf(Tag.class)));
        assertThat(symfonie.findTags(queryBuilder().filter(eq("IsFinance", true)).build()), not(emptyCollectionOf(Tag.class)));
        assertThat(symfonie.findTags(queryBuilder().filter(eq("Id", NON_EXISTENT_ID)).build()), emptyCollectionOf(Tag.class));
    }


    @Test
    public void testGetProject() throws Exception {
        assertThat(symfonie.getProject(1l), notNullValue());
        assertThat(symfonie.getProject(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testGetUserProjects() throws Exception {
        assertThat(symfonie.getUserProjects(REQUESTOR_ID), not(emptyCollectionOf(Project.class)));
    }

    @Test
    public void testFindProjects() throws Exception {
        Query query = queryBuilder().filter(startsWith("Name", "a")).top(5).build();
        assertThat(symfonie.findProjects(query), not(emptyCollectionOf(Project.class)));
    }


    @Test
    public void testGetProjectCompany() throws Exception {
        assertThat(symfonie.getProjectCompany(1l), notNullValue());
        assertThat(symfonie.getProjectCompany(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindProjectCompanies() throws Exception {
        Query query = queryBuilder().filter(eq("ProjectId", 82)).build();
        assertThat(symfonie.findProjectCompanies(query), not(emptyCollectionOf(ProjectCompany.class)));
    }


    @Test
    public void testGetJob() throws Exception {
        Job job = symfonie.getJob(1l);
        assertThat(job, notNullValue());
        assertThat(job.getWorkflows(), not(emptyCollectionOf(Workflow.class)));

        assertThat(symfonie.getJob(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindJobs() throws Exception {
        Query query = queryBuilder().expand("Workflows").filter(startsWith("Name", "a")).top(10).build();
        assertThat(symfonie.findJobs(query), not(emptyCollectionOf(Job.class)));
    }

    @Test
    public void testCreateJob() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"));

        Job created = symfonie.createJob(job);

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());

        assertThat(created.getName(), equalTo(job.getName()));
        assertThat(created.getState(), equalTo(job.getState()));
        assertThat(created.getProjectId(), equalTo(job.getProjectId()));
        assertThat(created.getRequestorId(), equalTo(job.getRequestorId()));
        assertThat(created.getSourceLanguageCode(), equalTo(job.getSourceLanguageCode()));
        assertThat(created.getTargetLanguageCodes(), equalTo(job.getTargetLanguageCodes()));
    }

    @Test
    public void testCreateJobWithExternalId() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"), "external" + DateTime.now());

        Job created = symfonie.createJob(job);

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());

        assertThat(created.getName(), equalTo(job.getName()));
        assertThat(created.getState(), equalTo(job.getState()));
        assertThat(created.getProjectId(), equalTo(job.getProjectId()));
        assertThat(created.getRequestorId(), equalTo(job.getRequestorId()));
        assertThat(created.getSourceLanguageCode(), equalTo(job.getSourceLanguageCode()));
        assertThat(created.getTargetLanguageCodes(), equalTo(job.getTargetLanguageCodes()));
        assertThat(created.getExternalId(), equalTo(job.getExternalId()));
    }

    private Job createJob(String name, long projectId, long requestorId, String sourceLanguageCode, List<String> targetLanguageCodes, String externalId) {
        Job job = new Job();
        job.setName(name);
        job.setState(Job.State.Draft);
        job.setProjectId(projectId);
        job.setRequestorId(requestorId);
        job.setSourceLanguageCode(sourceLanguageCode);
        job.setTargetLanguageCodes(targetLanguageCodes);
        job.setExternalId(externalId);
        return job;
    }

    private Job createJob(String name, long projectId, long requestorId, String sourceLanguageCode, List<String> targetLanguageCodes) {
        return createJob(name, projectId, requestorId, sourceLanguageCode, targetLanguageCodes, null);
    }

    @Test
    public void testUpdateJob() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"));
        job = symfonie.createJob(job);

        job.setName("Updated " + DateTime.now());
        Job updated = symfonie.updateJob(job);

        assertThat(updated.getName(), equalTo(job.getName()));
    }

    @Test
    public void testUpdateJobWithExternalId() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"), "external" + DateTime.now());
        job = symfonie.createJob(job);

        job.setName("Updated " + DateTime.now());
        Job updated = symfonie.updateJob(job);

        assertThat(updated.getName(), equalTo(job.getName()));
        assertThat(updated.getExternalId(), equalTo(job.getExternalId()));
    }

    @Test
    public void testChangeJobState() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"));
        job = symfonie.createJob(job);

        assertThat(job.getState(), equalTo(Job.State.Draft));

        symfonie.doJobAction(job.getId(), JobAction.HeadsUp);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.HeadsUp));

        symfonie.doJobAction(job.getId(), JobAction.Order);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.Order));

        symfonie.doJobAction(job.getId(), JobAction.Accept);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.InProgress));

        symfonie.doJobAction(job.getId(), JobAction.Complete);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.Completed));

        symfonie.doJobAction(job.getId(), JobAction.Approve);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.Approved));

        symfonie.doJobAction(job.getId(), JobAction.Reopen);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.InProgress));

        symfonie.doJobAction(job.getId(), JobAction.Cancel);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.Canceled));
    }

    @Test
    @Ignore // asi to na workflow nakonec nepujde
    public void testChangeJobState_cancelInDraft() throws Exception {
        Job job = createJob("Test " + DateTime.now(), PROJECT_ID, REQUESTOR_ID, "en", Lists.newArrayList("cs", "sk"));
        job = symfonie.createJob(job);

        assertThat(job.getState(), equalTo(Job.State.Draft));

        symfonie.doJobAction(job.getId(), JobAction.Cancel);
        assertThat(symfonie.getJob(job.getId()).getState(), equalTo(Job.State.Canceled));
    }


    @Test
    public void testGetWorkflow() throws Exception {
        assertThat(symfonie.getWorkflow(1l), notNullValue());
        assertThat(symfonie.getWorkflow(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindWorkflows() throws Exception {
        Query query = queryBuilder().filter(eq("JobId", 1)).top(10).build();
        assertThat(symfonie.findWorkflows(query), not(emptyCollectionOf(Workflow.class)));
    }

    @Test
    public void testCreateWorkflow() throws Exception {
        Workflow workflow = createWorkflow(1l, "Test " + DateTime.now(), "en", "cs");

        Workflow created = symfonie.createWorkflow(workflow);

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());
        assertThat(created.getUpdatedAt(), notNullValue());

        assertThat(created.getJobId(), equalTo(workflow.getJobId()));
        assertThat(created.getName(), equalTo(workflow.getName()));
        assertThat(created.getSourceLanguageCode(), equalTo(workflow.getSourceLanguageCode()));
        assertThat(created.getTargetLanguageCode(), equalTo(workflow.getTargetLanguageCode()));
    }

    private Workflow createWorkflow(long jobId, String name, String sourceLanguageCode, String targetLanguageCode) {
        Workflow workflow = new Workflow();
        workflow.setName(name);
        workflow.setJobId(jobId);
        workflow.setSourceLanguageCode(sourceLanguageCode);
        workflow.setTargetLanguageCode(targetLanguageCode);
        return workflow;
    }

    @Test
    public void testUpdateWorkflow() throws Exception {
        Workflow workflow = createWorkflow(1l, "Test " + DateTime.now(), "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        workflow.setName("Changed " + DateTime.now());
        Workflow updated = symfonie.updateWorkflow(workflow);

        assertThat(updated.getName(), equalTo(workflow.getName()));
    }


    @Test
    public void testGetTask() throws Exception {
        Task task = symfonie.getTask(1l);
        assertThat(task, notNullValue());
        assertThat(task.getProject(), notNullValue());
        assertThat(task.getAttachments(), notNullValue());
        assertThat(task.getFinanceRows(), notNullValue());

        assertThat(symfonie.getTask(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindTasks() throws Exception {
        Query query = queryBuilder().filter(eq("JobId", 1)).top(10).build();
        assertThat(symfonie.findTasks(query), not(emptyCollectionOf(Task.class)));
    }

    @Test
    public void testCreateTask() throws Exception {
        DateTime now = DateTime.now();

        Workflow workflow = createWorkflow(1l, "Test workflow " + now, "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        Task task = createTask(1l, workflow.getId(), "Test task " + now, now.plusHours(1), now.plusHours(100));
        Task created = symfonie.createTask(task);

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getVersion(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());
        assertThat(created.getUpdatedAt(), notNullValue());
        assertThat(created.getState(), equalTo(Task.State.Draft));
        assertThat(created.getServiceTag(), notNullValue());
        assertThat(created.getSourceLanguageCode(), notNullValue());
        assertThat(created.getTargetLanguageCode(), notNullValue());
        assertThat(created.getServiceTag(), equalTo(task.getServiceTag()));

        assertThat(created.getName(), equalTo(task.getName()));
        assertThat(created.getRequestorId(), equalTo(task.getRequestorId()));
        assertThat(created.getWorkflowId(), equalTo(task.getWorkflowId()));
        assertThat(created.getJobId(), equalTo(task.getJobId()));
        assertThat(created.getStartDate(), equalTo(task.getStartDate()));
        assertThat(created.getDueDate(), equalTo(task.getDueDate()));
        assertThat(created.getDescription(), equalTo(task.getDescription()));
        assertThat(created.getTags(), equalTo(task.getTags()));
        assertThat(created.getType(), equalTo(task.getType()));
    }

    private Task createTask(long jobId, long workflowId, String name, DateTime startDate, DateTime dueDate) {
        Task task = new Task();
        task.setName(name);
        task.setRequestorId(REQUESTOR_ID);
        task.setWorkflowId(workflowId);
        task.setJobId(jobId);
        task.setStartDate(startDate);
        task.setDueDate(dueDate);
        task.setDescription("Text");
        task.setTags(Lists.newArrayList("a", "b", "c"));
        task.setType(Task.Type.Translation);
        task.setServiceTag("SW.Translation");
        return task;
    }


    @Test
    public void testUpdateTask() throws Exception {
        DateTime now = DateTime.now();

        Workflow workflow = createWorkflow(1l, "Test workflow " + now, "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        Task task = createTask(1l, workflow.getId(), "Test task " + now, now.plusHours(1), now.plusHours(100));
        task = symfonie.createTask(task);

        task.setName("Changed name " + DateTime.now());
        Task updated = symfonie.updateTask(task);
        assertThat(updated.getName(), equalTo(task.getName()));
    }

    @Test
    public void testAssignTask() throws Exception {
        DateTime now = DateTime.now();

        Workflow workflow = createWorkflow(1l, "Test workflow " + now, "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        Task task = createTask(1l, workflow.getId(), "Test task " + now, now.plusHours(1), now.plusHours(100));
        task = symfonie.createTask(task);

        List<Long> someUserIds = symfonie.findUsers(queryBuilder().top(2).build()).stream()
                .map(User::getId).collect(toList());
        task.setAssignees(someUserIds.stream().map(Assignee::forUserId).collect(toList()));
        Task updated = symfonie.updateTask(task);

        assertThat(updated.getAssignees().stream().map(Assignee::getUserId).collect(toList()), equalTo(someUserIds));
    }

    @Test
    public void testChangeTaskState() throws Exception {
        DateTime now = DateTime.now();

        Workflow workflow = createWorkflow(1l, "Test workflow " + now, "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        Task task = createTask(1l, workflow.getId(), "Test task " + now, now.plusHours(1), now.plusHours(100));
        task = symfonie.createTask(task);

        task = symfonie.doTaskAction(task.getId(), TaskAction.HeadsUp);
        assertThat(task.getState(), equalTo(Task.State.HeadsUp));

        task = symfonie.doTaskAction(task.getId(), TaskAction.Claim);
        assertThat(task.getState(), equalTo(Task.State.HeadsUpClaimed));

        task = symfonie.doTaskAction(task.getId(), TaskAction.Order);
        assertThat(task.getState(), equalTo(Task.State.Order));
        assertThat(task.getOrderDate(), notNullValue());

        task = symfonie.doTaskAction(task.getId(), TaskAction.Accept);
        assertThat(task.getState(), equalTo(Task.State.InProgress));
        assertThat(task.getAcceptedDate(), notNullValue());

        task = symfonie.doTaskAction(task.getId(), TaskAction.Complete);
        assertThat(task.getState(), equalTo(Task.State.Completed));

        task = symfonie.doTaskAction(task.getId(), TaskAction.Approve);
        assertThat(task.getState(), equalTo(Task.State.Approved));

        task = symfonie.doTaskAction(task.getId(), TaskAction.Reopen);
        assertThat(task.getState(), equalTo(Task.State.InProgress));

        task = symfonie.doTaskAction(task.getId(), TaskAction.Cancel);
        assertThat(task.getState(), equalTo(Task.State.Canceled));
    }

    @Test
    @Ignore // asi to na workflow nakonec nepujde
    public void testChangeTaskState_cancelInDraft() throws Exception {
        DateTime now = DateTime.now();

        Workflow workflow = createWorkflow(1l, "Test workflow " + now, "en", "cs");
        workflow = symfonie.createWorkflow(workflow);

        Task task = createTask(1l, workflow.getId(), "Test task " + now, now.plusHours(1), now.plusHours(100));
        task = symfonie.createTask(task);

        assertThat(task.getState(), equalTo(Task.State.Draft));
        task = symfonie.doTaskAction(task.getId(), TaskAction.Cancel);
        assertThat(task.getState(), equalTo(Task.State.Canceled));
    }


    @Test
    public void testGetJobComment() throws Exception {
        assertThat(symfonie.getJobComment(1l), notNullValue());
        assertThat(symfonie.getJobComment(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindJobComments() throws Exception {
        Query query = queryBuilder().filter(eq("JobId", 5432)).build();
        assertThat(symfonie.findJobComments(query), not(emptyCollectionOf(JobComment.class)));
    }

    @Test
    public void testCreateJobComment() throws Exception {
        JobComment comment = new JobComment();
        comment.setJobId(1l);
        comment.setMessage("Test " + DateTime.now());
        comment.setCreatedBy(me.getId());

        JobComment created = symfonie.createJobComment(comment);
        assertThat(created.getJobId(), equalTo(comment.getJobId()));
        assertThat(created.getCreatedBy(), equalTo(comment.getCreatedBy()));
        assertThat(created.getMessage(), equalTo(comment.getMessage()));

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());
        assertThat(created.getUpdatedAt(), notNullValue());
        assertThat(created.getCreatorEmail(), notNullValue());
        assertThat(created.getCreatorLogin(), notNullValue());
        assertThat(created.getCreatorName(), notNullValue());
    }


    @Test
    public void testGetTaskComment() throws Exception {
        assertThat(symfonie.getTaskComment(1l), notNullValue());
        assertThat(symfonie.getTaskComment(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindTaskComments() throws Exception {
        Query query = queryBuilder().filter(eq("TaskId", 1)).build();
        assertThat(symfonie.findTaskComments(query), not(emptyCollectionOf(TaskComment.class)));
    }

    @Test
    public void testCreateTaskComment() throws Exception {
        TaskComment comment = new TaskComment();
        comment.setCreatedBy(me.getId());
        comment.setTaskId(2l);
        comment.setMessage("Test " + DateTime.now());

        TaskComment created = symfonie.createTaskComment(comment);
        assertThat(created.getTaskId(), equalTo(comment.getTaskId()));
        assertThat(created.getCreatedBy(), equalTo(comment.getCreatedBy()));
        assertThat(created.getMessage(), equalTo(comment.getMessage()));

        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getCreatedAt(), notNullValue());
        assertThat(created.getUpdatedAt(), notNullValue());
        assertThat(created.getCreatorEmail(), notNullValue());
        assertThat(created.getCreatorLogin(), notNullValue());
        assertThat(created.getCreatorName(), notNullValue());
    }


    @Test
    public void testGetJobAttachment() throws Exception {
        assertThat(symfonie.getJobAttachment(10000001l), notNullValue());
        assertThat(symfonie.getJobAttachment(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testGetTaskAttachment() throws Exception {
        assertThat(symfonie.getTaskAttachment(2l), notNullValue());
        assertThat(symfonie.getTaskAttachment(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testFindJobAttachments() throws Exception {
        Query query = queryBuilder().filter(eq("JobId", 4121)).build();
        assertThat(symfonie.findJobAttachments(query), not(emptyCollectionOf(JobAttachment.class)));
    }

    @Test
    public void testFindTaskAttachments() throws Exception {
        Query query = queryBuilder().filter(eq("TaskId", 1)).build();
        assertThat(symfonie.findTaskAttachments(query), not(emptyCollectionOf(TaskAttachment.class)));
    }

    @Test
    public void testUploadJobAttachment() throws Exception {
        File file = createAttachmentFile("input.txt", "Text");
        JobAttachment attachment = symfonie.uploadJobAttachment(1l, file, Type.Other, "input.txt", "de");

        assertThat(attachment, notNullValue());
        assertThat(attachment.getJobId(), equalTo(1l));
        assertThat(attachment.getType(), equalTo(Type.Other));
        assertThat(attachment.getName(), equalTo(file.getName()));
        assertThat(attachment.getTargetLanguageCode(), equalTo("de"));

        assertThat(file.delete(), is(true));
    }

    @Test
    public void testUploadTaskAttachment() throws Exception {
        File file = createAttachmentFile("input.txt", "Text");
        TaskAttachment attachment = symfonie.uploadTaskAttachment(1l, file, Type.Other, "input.txt");

        assertThat(attachment, notNullValue());
        assertThat(attachment.getTaskId(), equalTo(1l));
        assertThat(attachment.getType(), equalTo(Type.Other));
        assertThat(attachment.getName(), equalTo(file.getName()));

        assertThat(file.delete(), is(true));
    }

    @Test
    public void testDownloadJobAttachment() throws Exception {
        JobAttachment attachment = symfonie.getJobAttachment(10000008l);
        File file = symfonie.downloadJobAttachment(attachment, new File("."), attachment.getName());
        assertThat(file, notNullValue());
        assertThat(file.getName(), equalTo(attachment.getName()));
        assertThat(file.delete(), is(true));
    }

    @Test
    public void testDownloadTaskAttachment() throws Exception {
        TaskAttachment attachment = symfonie.getTaskAttachment(2l);
        File file = symfonie.downloadTaskAttachment(attachment, new File("."), attachment.getName());
        assertThat(file, notNullValue());
        assertThat(file.getName(), equalTo(attachment.getName()));
        assertThat(file.delete(), is(true));
    }

    @Test
    public void testDeleteJobAttachment() throws Exception {
        File file = createAttachmentFile("input.txt", "text");
        JobAttachment attachment = symfonie.uploadJobAttachment(1l, file, Type.Other, "input.txt");

        symfonie.deleteJobAttachment(attachment.getId());
        assertThat(symfonie.getJobAttachment(attachment.getId()), nullValue());
        assertThat(file.delete(), is(true));
    }

    @Test
    public void testDeleteTaskAttachment() throws Exception {
        File file = createAttachmentFile("input.txt", "text");
        TaskAttachment attachment = symfonie.uploadTaskAttachment(1l, file, Type.Other, "input.txt");

        symfonie.deleteTaskAttachment(attachment.getId());
        assertThat(symfonie.getTaskAttachment(attachment.getId()), nullValue());
        assertThat(file.delete(), is(true));
    }

    private File createAttachmentFile(String name, String content) throws FileNotFoundException {
        File file = new File(name);
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(file))) {
            writer.print(content);
        }
        return file;
    }


    @Test
    public void testGetFinanceRow() throws Exception {
        FinanceRow row = symfonie.getFinanceRow(1001085l);
        assertThat(row, notNullValue());
        assertThat(row.getPrices(), notNullValue());

        assertThat(symfonie.getFinanceRow(NON_EXISTENT_ID), nullValue());
    }

    @Test
    public void testGetFinanceRows() throws Exception {
        assertThat(symfonie.getFinanceRows(1001085l), not(emptyCollectionOf(FinanceRow.class)));
    }

    @Test
    public void testFindFinanceRows() throws Exception {
        Query query = queryBuilder().filter(eq("TaskId", 1001085)).build();
        assertThat(symfonie.findFinanceRows(query), not(emptyCollectionOf(FinanceRow.class)));
    }

    @Test
    public void testCreateFinanceRow() throws Exception {
        FinanceRow row = createFinanceRow(1l, "10.0", "1.0", false, "123");

        FinanceRow created = symfonie.createFinanceRow(row);
        assertThat(created.getId(), greaterThan(0l));
        assertThat(created.getUpdatedAt(), notNullValue());
        assertThat(created.getMinUsd(), notNullValue());
        assertThat(created.getMaxUsd(), notNullValue());
        assertThat(created.getSegmentId(), notNullValue());
        assertThat(created.getRequestorLogin(), notNullValue());

        assertThat(created.getBillingUnit(), equalTo(row.getBillingUnit()));
        assertThat(created.isBillable(), equalTo(row.isBillable()));
        assertThat(created.getQuantity().setScale(10), equalTo(row.getQuantity().setScale(10)));
        assertThat(created.getDiscount().setScale(10), equalTo(row.getDiscount().setScale(10)));
        assertThat(created.getPoNumber(), equalTo(row.getPoNumber()));
        assertThat(created.getTaskId(), equalTo(row.getTaskId()));
        assertThat(created.getPartnerType(), equalTo(row.getPartnerType()));
        assertThat(created.getAttachmentId(), equalTo(row.getAttachmentId()));
        assertThat(created.getActivityNo(), equalTo(row.getActivityNo()));
    }

    @Test
    public void testUpdateFinanceRow() throws Exception {
        FinanceRow row = symfonie.createFinanceRow(createFinanceRow(1l, "10.0", "1.0", false, null)); // po-number must not be set to allow row to be updated

        row.setBillable(true);
        row.setQuantity(new BigDecimal("20.0"));
        row.setDiscount(new BigDecimal("5.0"));
        FinanceRow updated = symfonie.updateFinanceRow(row);

        assertThat(updated.isBillable(), is(true));
        assertThat(updated.getQuantity().setScale(10), equalTo(new BigDecimal("20.0").setScale(10)));
        assertThat(updated.getDiscount().setScale(10), equalTo(new BigDecimal("5.0").setScale(10)));
    }

    @Test
    public void testDeleteFinanceRow() throws Exception {
        FinanceRow row = symfonie.createFinanceRow(createFinanceRow(1l, "10.0", "1.0", false, null));
        assertThat(symfonie.getFinanceRow(row.getId()), notNullValue());

        symfonie.deleteFinanceRow(row.getId());
        assertThat(symfonie.getFinanceRow(row.getId()), nullValue());
    }

    private FinanceRow createFinanceRow(long taskId, String quantity, String discount, boolean billable, String poNumber) {
        FinanceRow row = new FinanceRow();
        row.setTaskId(taskId);
        row.setBillingUnit(FinanceRow.BillingUnit.Word);
        row.setQuantity(new BigDecimal(quantity));
        row.setDiscount(new BigDecimal(discount));
        row.setBillable(billable);
        row.setPartnerType(FinanceRow.PartnerType.Creator);
        row.setActivityNo("123");
        row.setPoNumber(poNumber);
        return row;
    }


    @Test
    public void testGetProjectVendors() throws Exception {
        List<User> users = symfonie.getProjectVendors(1l, "de", 1, false, null);
        assertThat(users, not(emptyCollectionOf(User.class)));
    }
}