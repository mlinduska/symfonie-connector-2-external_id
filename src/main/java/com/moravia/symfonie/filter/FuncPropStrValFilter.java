package com.moravia.symfonie.filter;

public class FuncPropStrValFilter extends BaseFilter {
    private String function;
    private String property;
    private String value;

    public FuncPropStrValFilter(String function, String property, String value) {
        this.function = function;
        this.property = property;
        this.value = value;
    }

    @Override
    public String getFilterString() {
        return String.format("%s(%s, '%s')", function, property, value);
    }
}
