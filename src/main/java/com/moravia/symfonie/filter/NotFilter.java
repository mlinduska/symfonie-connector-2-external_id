package com.moravia.symfonie.filter;

public class NotFilter extends BaseFilter {
    private Filter filter;

    public NotFilter(Filter filter) {
        this.filter = filter;
    }

    @Override
    public String getFilterString() {
        return "not (" + filter.getFilterString() + ")";
    }
}
