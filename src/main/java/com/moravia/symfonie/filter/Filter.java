package com.moravia.symfonie.filter;


public interface Filter {
    String getFilterString();
}
