package com.moravia.symfonie.filter;


public class TextFilter extends BaseFilter {
    private String filter;

    public TextFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String getFilterString() {
        return filter;
    }
}
