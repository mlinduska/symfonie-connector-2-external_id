package com.moravia.symfonie.filter;


import java.util.List;

import static java.util.stream.Collectors.joining;

public class AndFilter extends BaseFilter {
    private List<Filter> parts;

    public AndFilter(List<Filter> parts) {
        this.parts = parts;
    }

    @Override
    public String getFilterString() {
        if (parts.isEmpty()) return "true";
        else if (parts.size() == 1) return parts.get(0).getFilterString();
        else return parts.stream().map(i -> "(" + i.getFilterString() + ")").collect(joining(" and "));
    }

}
