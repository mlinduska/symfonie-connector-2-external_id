package com.moravia.symfonie.filter;


import com.google.common.collect.Lists;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class Filters {
    public static Filter text(String text) {
        return new TextFilter(text);
    }

    public static Filter and(List<Filter> parts) {
        return new AndFilter(parts);
    }

    public static Filter and(Filter... parts) {
        return and(Lists.newArrayList(parts));
    }

    public static Filter or(List<Filter> parts) {
        return new OrFilter(parts);
    }

    public static Filter or(Filter... parts) {
        return or(Lists.newArrayList(parts));
    }

    public static Filter not(Filter filter) {
        return new NotFilter(filter);
    }

    public static <T> Filter eq(String property, T value) {
        return new PropOpValFilter<>(property, "eq", value);
    }

    public static <T> Filter gt(String property, T value) {
        return new PropOpValFilter<>(property, "gt", value);
    }

    public static <T> Filter ge(String property, T value) {
        return new PropOpValFilter<>(property, "ge", value);
    }

    public static <T> Filter lt(String property, T value) {
        return new PropOpValFilter<>(property, "lt", value);
    }

    public static <T> Filter le(String property, T value) {
        return new PropOpValFilter<>(property, "le", value);
    }

    public static <T> Filter contains(String property, String value) {
        return new FuncPropStrValFilter("contains", property, value);
    }

    public static <T> Filter startsWith(String property, String value) {
        return new FuncPropStrValFilter("startswith", property, value);
    }

    public static <T> Filter endsWith(String property, String value) {
        return new FuncPropStrValFilter("endswith", property, value);
    }

    public static <T> Filter in(String property, List<T> values) {
        return or(values.stream().map(i -> eq(property, i)).collect(toList()));
    }

    public static <T> Filter in(String property, T... values) {
        return in(property, Lists.newArrayList(values));
    }


}
