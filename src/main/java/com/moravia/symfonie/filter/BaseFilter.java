package com.moravia.symfonie.filter;


public abstract class BaseFilter implements Filter {
    @Override
    public String toString() {
        return getFilterString();
    }
}
