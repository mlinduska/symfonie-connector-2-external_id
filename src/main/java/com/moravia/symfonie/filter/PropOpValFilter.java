package com.moravia.symfonie.filter;


import com.moravia.symfonie.domain.odata.ODataEnum;

public class PropOpValFilter<T> extends BaseFilter {
    private String property;
    private String operation;
    private T value;

    public PropOpValFilter(String property, String operation, T value) {
        this.property = property;
        this.operation = operation;
        this.value = value;
    }

    @Override
    public String getFilterString() {
        if (String.class.isAssignableFrom(value.getClass())) {
            return String.format("%s %s '%s'", property, operation, value.toString());
        } else if (ODataEnum.class.isAssignableFrom(value.getClass())) {
            ODataEnum en = (ODataEnum) value;
            return String.format("%s %s %s'%s'", property, operation, en.getODataPrefix(), en.getJsonValue());
        } else {
            return String.format("%s %s %s", property, operation, value.toString());

        }
    }
}
