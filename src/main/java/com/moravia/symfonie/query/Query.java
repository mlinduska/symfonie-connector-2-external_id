package com.moravia.symfonie.query;

import com.moravia.symfonie.filter.Filter;
import org.apache.http.NameValuePair;

import java.util.Collection;


public interface Query {

    Filter getFilter();

    Ordering getOrdering();

    Integer getTop();

    Integer getSkip();

    Collection<String> getExpands();

    Collection<String> getSelects();

    NameValuePair[] getUriParams();
}
