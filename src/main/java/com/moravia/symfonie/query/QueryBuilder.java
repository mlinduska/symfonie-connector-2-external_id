package com.moravia.symfonie.query;

import com.google.common.collect.Sets;
import com.moravia.symfonie.filter.Filter;

import java.util.Collection;

import static com.moravia.symfonie.query.Ordering.Direction;


public class QueryBuilder {
    private static final Query EMPTY_FILTER = new QueryBuilder().build();

    private Filter filter;
    private Ordering ordering;
    private Integer top;
    private Integer skip;
    private Collection<String> expands = Sets.newLinkedHashSet();
    private Collection<String> selects = Sets.newLinkedHashSet();

    public static Query emptyQuery() {
        return EMPTY_FILTER;
    }

    public static QueryBuilder queryBuilder() {
        return new QueryBuilder();
    }

    public Query build() {
        return new QueryImpl(filter, ordering, top, skip, expands, selects);
    }

    public QueryBuilder filter(Filter filter) {
        this.filter = filter;
        return this;
    }

    public QueryBuilder orderBy(String property) {
        this.ordering = new Ordering(property, Direction.asc);
        return this;
    }

    public QueryBuilder orderBy(String property, Direction dir) {
        this.ordering = new Ordering(property, dir);
        return this;
    }

    public QueryBuilder top(Integer top) {
        this.top = top;
        return this;
    }

    public QueryBuilder skip(Integer skip) {
        this.skip = skip;
        return this;
    }

    public QueryBuilder expand(String property) {
        expands.add(property);
        return this;
    }

    public QueryBuilder select(String property) {
        selects.add(property);
        return this;
    }
}
