package com.moravia.symfonie.query;


public class Ordering {
    public enum Direction {
        asc, desc
    }

    private String property;
    private Direction direction;

    public Ordering(String property, Direction direction) {
        this.property = property;
        this.direction = direction;
    }

    public String getProperty() {
        return property;
    }

    public Direction getDirection() {
        return direction;
    }

    @Override
    public String toString() {
        return String.format("(%s %s)", property, direction);
    }
}
