package com.moravia.symfonie.query;

import com.google.common.collect.Lists;
import com.moravia.symfonie.filter.Filter;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.joining;

public class QueryImpl implements Query {
    private Filter filter;
    private Ordering ordering;
    private Integer top;
    private Integer skip;
    private Collection<String> expands = Collections.emptyList();
    private Collection<String> selects = Collections.emptyList();

    public QueryImpl(Filter filter, Ordering ordering, Integer top, Integer skip, Collection<String> expands, Collection<String> selects) {
        this.filter = filter;
        this.ordering = ordering;
        this.top = top;
        this.skip = skip;
        this.expands = expands;
        this.selects = selects;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    @Override
    public Ordering getOrdering() {
        return ordering;
    }

    @Override
    public Integer getTop() {
        return top;
    }

    @Override
    public Integer getSkip() {
        return skip;
    }

    @Override
    public Collection<String> getExpands() {
        return expands;
    }

    @Override
    public Collection<String> getSelects() {
        return selects;
    }

    @Override
    public NameValuePair[] getUriParams() {
        List<NameValuePair> result = Lists.newArrayList();
        if (filter != null) result.add(new BasicNameValuePair("$filter", filter.toString()));
        if (ordering != null)
            result.add(new BasicNameValuePair("$orderby", String.format("%s %s", ordering.getProperty(), ordering.getDirection().name())));
        if (top != null) result.add(new BasicNameValuePair("$top", Integer.toString(top)));
        if (skip != null) result.add(new BasicNameValuePair("$skip", Integer.toString(skip)));
        if (!expands.isEmpty())
            result.add(new BasicNameValuePair("$expand", expands.stream().collect(joining(","))));
        if (!selects.isEmpty())
            result.add(new BasicNameValuePair("$select", selects.stream().collect(joining(","))));
        return result.toArray(new NameValuePair[result.size()]);
    }

    @Override
    public String toString() {
        return "QueryImpl{" +
                "filter=" + filter +
                ", ordering=" + ordering +
                ", top=" + top +
                ", skip=" + skip +
                ", expands=" + expands +
                ", selects=" + selects +
                '}';
    }

}
