package com.moravia.symfonie.domain.odata;


import com.fasterxml.jackson.annotation.JsonValue;

public interface ODataEnum {
    @JsonValue
    String getJsonValue();

    String getODataPrefix();
}
