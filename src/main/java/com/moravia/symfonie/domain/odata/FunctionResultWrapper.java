package com.moravia.symfonie.domain.odata;

import com.fasterxml.jackson.annotation.JsonProperty;


public class FunctionResultWrapper<T> {
    @JsonProperty("value")
    private T value;

    public T getValue() {
        return value;
    }
}
