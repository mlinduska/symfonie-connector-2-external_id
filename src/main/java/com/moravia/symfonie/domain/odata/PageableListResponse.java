package com.moravia.symfonie.domain.odata;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;
import java.util.List;


public class PageableListResponse<T> {
    @JsonProperty("value")
    protected List<T> values;
    @JsonProperty("@odata.nextLink")
    protected URI nextPageLink;

    public URI getNextPageLink() {
        return nextPageLink;
    }

    public List<T> getValues() {
        return values;
    }
}