package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Lists;
import com.moravia.symfonie.domain.odata.Expandable;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;
import org.joda.time.DateTime;

import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;

public class Job {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(CreateUpdate.class)
    private String name;

    @Nonnull
    @JsonProperty("ProjectId")
    @JsonView(CreateUpdate.class)
    private long projectId;

    @JsonProperty("DueDate")
    @JsonView(CreateUpdate.class)
    private DateTime dueDate;

    @JsonProperty("Description")
    @JsonView(CreateUpdate.class)
    private String description;

    @Nonnull
    @JsonProperty("State")
    @JsonView(Read.class)
    private State state = State.Draft;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @Nonnull
    @JsonProperty("RequestorId")
    @JsonView(CreateUpdate.class)
    private long requestorId;

    @Nonnull
    @JsonProperty("SourceLanguageCode")
    @JsonView(CreateUpdate.class)
    private String sourceLanguageCode;

    @JsonProperty("ExternalId")
    @JsonView(CreateUpdate.class)
    private String externalId;


    @JsonProperty("TargetLanguageCodes")
    @JsonView(CreateUpdate.class)
    private List<String> targetLanguageCodes = Lists.newArrayList();

    @Expandable
    @JsonProperty("Workflows")
    @JsonView(Read.class)
    private List<Workflow> workflows = Lists.newArrayList();

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }


    public enum State implements ODataEnum {
        Draft(0),
        Order(1),
        InProgress(2),
        Completed(3),
        Rejected(4),
        Canceled(5),
        OnHold(6),
        Approved(7),
        HeadsUp(8);

        private int code;

        State(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.JobState";
        }

        public int getCode() {
            return code;
        }
    }

    public long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }


    public long getRequestorId() {
        return requestorId;
    }

    public void setRequestorId(long requestorId) {
        this.requestorId = requestorId;
    }

    public String getSourceLanguageCode() {
        return sourceLanguageCode;
    }

    public void setSourceLanguageCode(String sourceLanguageCode) {
        this.sourceLanguageCode = sourceLanguageCode;
    }

    public List<String> getTargetLanguageCodes() {
        return targetLanguageCodes;
    }

    public void setTargetLanguageCodes(List<String> targetLanguageCodes) {
        this.targetLanguageCodes = targetLanguageCodes;
    }

    public List<Workflow> getWorkflows() {
        return workflows;
    }


}
