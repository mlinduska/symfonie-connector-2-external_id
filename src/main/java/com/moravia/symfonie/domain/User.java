package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.moravia.symfonie.domain.odata.Expandable;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;
import com.moravia.symfonie.util.json.UserTimezoneDeserializer;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class User {

    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(Read.class)
    private String name;

    @JsonProperty("CompanyCode")
    @JsonView(Read.class)
    private String companyCode;

    @Nonnull
    @JsonProperty("GlobalRole")
    @JsonView(Read.class)
    private Role role;

    @Nonnull
    @JsonProperty("AdditionalRoles")
    @JsonView(Read.class)
    private AdditionalRole additionalRole;

    @Nonnull
    @JsonProperty("IsActive")
    @JsonView(Read.class)
    private boolean active;

    @JsonProperty("Login")
    @JsonView(Read.class)
    private String login;

    @JsonProperty("Position")
    @JsonView(Read.class)
    private String position;

    @Nonnull
    @JsonProperty("UserType")
    @JsonView(Read.class)
    private Type userType;

    @JsonProperty("TimeZone")
    @JsonView(Read.class)
    private String timeZoneName;

    @JsonProperty("TimeZoneUtcOffset")
    @JsonDeserialize(using = UserTimezoneDeserializer.class)
    @JsonView(Read.class)
    private DateTimeZone timeZone; // format: [-]HH:mm:ss

    @Nonnull
    @JsonProperty("PageLimit")
    @JsonView(Read.class)
    private long pageLimit;

    @JsonProperty("Email")
    @JsonView(Read.class)
    private String email;

    @JsonProperty("Culture")
    @JsonView(Read.class)
    private String culture;

    @JsonProperty("CompanyName")
    @JsonView(Read.class)
    private String companyName;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @Nonnull
    @JsonProperty("WorkingTimes")
    @JsonView(Read.class)
    private List<Integer> workingTimes;

    @Nonnull
    @JsonProperty("SubscribedProjectIds")
    @JsonView(Read.class)
    private List<Long> subscribedProjectIds;

    @JsonProperty("CompanyId")
    @JsonView(Read.class)
    private Long companyId;

    @Expandable
    @JsonProperty("Vacations")
    @JsonView(Read.class)
    private List<Vacation> vacations;


    public enum Role implements ODataEnum {
        Member(0),
        Manager(1),
        Admin(2),;

        private int code;

        Role(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.GlobalRoleType";
        }

        public int getCode() {
            return code;
        }
    }

    public enum AdditionalRole implements ODataEnum {
        None(0),
        MLS_manager(1),;

        private int code;

        AdditionalRole(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.AdditionalRoleTypes";
        }

        public int getCode() {
            return code;
        }
    }

    public enum Type implements ODataEnum {
        Internal(0),
        Customer(1),
        Vendor(2),;

        private int code;

        Type(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.UserTypes";
        }

        public int getCode() {
            return code;
        }
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public Role getRole() {
        return role;
    }

    public AdditionalRole getAdditionalRole() {
        return additionalRole;
    }

    public boolean isActive() {
        return active;
    }

    public String getLogin() {
        return login;
    }

    public String getPosition() {
        return position;
    }

    public Type getUserType() {
        return userType;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public long getPageLimit() {
        return pageLimit;
    }

    public String getEmail() {
        return email;
    }

    public String getCulture() {
        return culture;
    }

    public String getCompanyName() {
        return companyName;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public List<Integer> getWorkingTimes() {
        return workingTimes;
    }

    public List<Long> getSubscribedProjectIds() {
        return subscribedProjectIds;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public List<Vacation> getVacations() {
        return vacations;
    }

    public DateTimeZone getTimeZone() {
        return timeZone;
    }
}
