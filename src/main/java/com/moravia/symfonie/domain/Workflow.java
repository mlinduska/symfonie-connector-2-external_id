package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import org.joda.time.DateTime;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;

public class Workflow {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @Nonnull
    @JsonProperty("UpdatedAt")
    @JsonView(Read.class)
    private DateTime updatedAt;

    @JsonProperty("Name")
    @JsonView(CreateUpdate.class)
    private String name;

    @Nonnull
    @JsonProperty("JobId")
    @JsonView(CreateUpdate.class)
    private long jobId;

    @JsonProperty("SourceLanguageCode")
    @JsonView(CreateUpdate.class)
    private String sourceLanguageCode;

    @Nonnull
    @JsonProperty("TargetLanguageCode")
    @JsonView(CreateUpdate.class)
    private String targetLanguageCode;


    public long getId() {
        return id;
    }


    public DateTime getCreatedAt() {
        return createdAt;
    }


    public DateTime getUpdatedAt() {
        return updatedAt;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getSourceLanguageCode() {
        return sourceLanguageCode;
    }

    public void setSourceLanguageCode(String sourceLanguageCode) {
        this.sourceLanguageCode = sourceLanguageCode;
    }

    public String getTargetLanguageCode() {
        return targetLanguageCode;
    }

    public void setTargetLanguageCode(String targetLanguageCode) {
        this.targetLanguageCode = targetLanguageCode;
    }
}
