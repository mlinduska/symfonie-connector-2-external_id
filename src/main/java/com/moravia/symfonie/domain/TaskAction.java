package com.moravia.symfonie.domain;


import com.moravia.symfonie.domain.odata.ODataEnum;

public enum TaskAction implements ODataEnum {
    HeadsUp(0),
    Order(1),
    Claim(2),
    Accept(3),
    Reject(4),
    Complete(5),
    Cancel(6),
    Reopen(7),
    Archive(8),
    Approve(9),
    Unarchive(10),
    OnHold(11),;

    private int code;

    TaskAction(int code) {
        this.code = code;
    }

    @Override
    public String getJsonValue() {
        return name();
    }

    @Override
    public String getODataPrefix() {
        return "Moravia.Symfonie.Business.Services.TaskCommand";
    }

    public int getCode() {
        return code;
    }
}
