package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import org.joda.time.DateTime;

import java.math.BigDecimal;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class Price {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @Nonnull
    @JsonProperty("CalculatedQuantity")
    @JsonView(Read.class)
    private BigDecimal calculatedQuantity;

    @Nonnull
    @JsonProperty("PartnerId")
    @JsonView(Read.class)
    private long partnerId;

    @JsonProperty("PartnerCode")
    @JsonView(Read.class)
    private String partnerCode;

    @JsonProperty("PartnerCurrency")
    @JsonView(Read.class)
    private String partnerCurrency;

    @JsonProperty("PartnerName")
    @JsonView(Read.class)
    private String partnerName;

    @Nonnull
    @JsonProperty("PartnerPrice")
    @JsonView(Read.class)
    private BigDecimal partnerPrice;

    @Nonnull
    @JsonProperty("UnitCost")
    @JsonView(Read.class)
    private BigDecimal unitCost;

    @Nonnull
    @JsonProperty("UsdPrice")
    @JsonView(Read.class)
    private BigDecimal usdPrice;

    @Nonnull
    @JsonProperty("UsdUnitCost")
    @JsonView(Read.class)
    private BigDecimal usdUnitCost;

    public long getId() {
        return id;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public BigDecimal getCalculatedQuantity() {
        return calculatedQuantity;
    }

    public long getPartnerId() {
        return partnerId;
    }

    public String getPartnerCode() {
        return partnerCode;
    }

    public String getPartnerCurrency() {
        return partnerCurrency;
    }

    public String getPartnerName() {
        return partnerName;
    }

    public BigDecimal getPartnerPrice() {
        return partnerPrice;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public BigDecimal getUsdPrice() {
        return usdPrice;
    }

    public BigDecimal getUsdUnitCost() {
        return usdUnitCost;
    }

}
