package com.moravia.symfonie.domain;


import com.moravia.symfonie.domain.odata.ODataEnum;

public enum JobAction implements ODataEnum {
    Order(0),
    Accept(1),
    Complete(2),
    Reopen(3),
    Reject(4),
    Cancel(5),
    OnHold(6),
    Approve(7),
    StartWorkflow(8),
    HeadsUp(9),;

    private int code;

    JobAction(int code) {
        this.code = code;
    }

    @Override
    public String getJsonValue() {
        return name();
    }

    @Override
    public String getODataPrefix() {
        return "Moravia.Symfonie.Business.JobCommand";
    }

    public int getCode() {
        return code;
    }
}
