package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class Tag {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(Read.class)
    private String name;

    @JsonProperty("Description")
    @JsonView(Read.class)
    private String description;

    @Nonnull
    @JsonProperty("IsFinance")
    @JsonView(Read.class)
    private boolean isFinance;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFinance() {
        return isFinance;
    }

    @Override
    public String toString() {
        return getName();
    }
}
