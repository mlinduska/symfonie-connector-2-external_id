package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import org.joda.time.DateTime;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class Vacation {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("StartDate")
    @JsonView(Read.class)
    private DateTime startDate;

    @Nonnull
    @JsonProperty("EndDate")
    @JsonView(Read.class)
    private DateTime endDate;

    @JsonProperty("Comment")
    @JsonView(Read.class)
    private String comment;

    public long getId() {
        return id;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public String getComment() {
        return comment;
    }
}
