package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;

import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class ProjectCompany {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("CompanyId")
    @JsonView(Read.class)
    private long companyId;

    @JsonProperty("LanguageCode")
    @JsonView(Read.class)
    private String languageCode;

    @Nonnull
    @JsonProperty("Position")
    @JsonView(Read.class)
    private long position;

    @Nonnull
    @JsonProperty("ProjectId")
    @JsonView(Read.class)
    private long projectId;

    @JsonProperty("Tags")
    @JsonView(Read.class)
    private List<String> tags;

    @JsonProperty("CompanyName")
    @JsonView(Read.class)
    private String companyName;

    @JsonProperty("CompanyCode")
    @JsonView(Read.class)
    private String companyCode;

    public long getId() {
        return id;
    }

    public long getCompanyId() {
        return companyId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public long getPosition() {
        return position;
    }

    public long getProjectId() {
        return projectId;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }
}
