package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;

public class JobAttachment extends BaseAttachment {
    @JsonProperty("JobId")
    @JsonView(Read.class)
    private Long jobId;
    @JsonProperty("TargetLanguageCode")
    @JsonView(CreateUpdate.class)
    private String targetLanguageCode;

    public Long getJobId() {
        return jobId;
    }

    public String getTargetLanguageCode() {
        return targetLanguageCode;
    }

    public void setTargetLanguageCode(String targetLanguageCode) {
        this.targetLanguageCode = targetLanguageCode;
    }
}
