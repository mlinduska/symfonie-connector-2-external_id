package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;
import org.joda.time.DateTime;

import static com.moravia.symfonie.domain.odata.Views.Read;

public abstract class BaseAttachment {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(Read.class)
    private String name;

    @Nonnull
    @JsonProperty("FileType")
    @JsonView(Read.class)
    private Type type;

    @JsonProperty("DownloadUrl")
    @JsonView(Read.class)
    private String downloadUrl;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @JsonProperty("MimeType")
    @JsonView(Read.class)
    private String mimeType;

    @JsonProperty("CheckSum")
    @JsonView(Read.class)
    private String checkSum;

    @Nonnull
    @JsonProperty("Size")
    @JsonView(Read.class)
    private long size;


    public enum Type implements ODataEnum {
        ProjectReference(0), TaskReference(1), Source(2), Target(3), Analysis(4), Other(5), QuoteReference(6),
        HandoffReference(7);

        private int code;

        Type(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }

        @JsonValue
        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.FileType";
        }
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public long getSize() {
        return size;
    }
}
