package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import org.joda.time.DateTime;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;


public class Assignee {
    @Nonnull
    @JsonProperty("UserId")
    @JsonView(CreateUpdate.class)
    private long userId;

    @Nonnull
    @JsonProperty("AssignedOn")
    @JsonView(Read.class)
    private DateTime assignedOn;

    public static Assignee forUser(User user) {
        return forUserId(user.getId());
    }

    public static Assignee forUserId(long userId) {
        Assignee assignee = new Assignee();
        assignee.setUserId(userId);
        return assignee;
    }

    public long getUserId() {
        return userId;
    }

    public DateTime getAssignedOn() {
        return assignedOn;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

}
