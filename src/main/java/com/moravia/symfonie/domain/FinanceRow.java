package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Expandable;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;


public class FinanceRow {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private Long id;

    @Nonnull
    @JsonProperty("BillingUnit")
    @JsonView(CreateUpdate.class)
    private BillingUnit billingUnit;

    @Nonnull
    @JsonProperty("IsBillable")
    @JsonView(CreateUpdate.class)
    private boolean billable;

    @Nonnull
    @JsonProperty("Quantity")
    @JsonView(CreateUpdate.class)
    private BigDecimal quantity;

    @Nonnull
    @JsonProperty("Discount")
    @JsonView(CreateUpdate.class)
    private BigDecimal discount;

    @JsonProperty("ErrorMessage")
    @JsonView(Read.class)
    private String errorMessage;

    @JsonProperty("PoNumber")
    @JsonView(CreateUpdate.class)
    private String poNumber;

    @Nonnull
    @JsonProperty("UpdatedAt")
    @JsonView(Read.class)
    private DateTime updatedAt;

    @Nonnull
    @JsonProperty("TaskId")
    @JsonView(CreateUpdate.class)
    private long taskId;

    @JsonProperty("ServiceTag")
    @JsonView(Read.class)
    private String serviceTag;

    @Nonnull
    @JsonProperty("PartnerType")
    @JsonView(CreateUpdate.class)
    private PartnerType partnerType;

    @Nonnull
    @JsonProperty("MinUsd")
    @JsonView(Read.class)
    private BigDecimal minUsd;

    @Nonnull
    @JsonProperty("MaxUsd")
    @JsonView(Read.class)
    private BigDecimal maxUsd;

    @JsonProperty("AttachmentId")
    @JsonView(CreateUpdate.class)
    private Long attachmentId;

    @JsonProperty("SegmentId")
    @JsonView(Read.class)
    private Long segmentId;

    @JsonProperty("ActivityNo")
    @JsonView(CreateUpdate.class)
    private String activityNo;

    @JsonProperty("RequestorLogin")
    @JsonView(Read.class)
    private String requestorLogin;

    @Expandable
    @JsonProperty("Prices")
    @JsonView(Read.class)
    private List<Price> prices;

    public enum BillingUnit implements ODataEnum {
        Hour(0),
        Page(1),
        Piece(2),
        Segment(3),
        Word(4),
        Percentage(5),;

        private int code;

        BillingUnit(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.BillingUnits";
        }

        public int getCode() {
            return code;
        }
    }

    public enum PartnerType implements ODataEnum {
        Creator(0),
        Assignee(1),;

        private int code;

        PartnerType(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.TaskRole";
        }

        public int getCode() {
            return code;
        }

    }

    public Long getId() {
        return id;
    }


    public BillingUnit getBillingUnit() {
        return billingUnit;
    }

    public void setBillingUnit(BillingUnit billingUnit) {
        this.billingUnit = billingUnit;
    }

    public boolean isBillable() {
        return billable;
    }

    public void setBillable(boolean billable) {
        this.billable = billable;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public String getErrorMessage() {
        return errorMessage;
    }


    public String getPoNumber() {
        return poNumber;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }


    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public String getServiceTag() {
        return serviceTag;
    }

    public PartnerType getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(PartnerType partnerType) {
        this.partnerType = partnerType;
    }

    public BigDecimal getMinUsd() {
        return minUsd;
    }


    public BigDecimal getMaxUsd() {
        return maxUsd;
    }


    public Long getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Long attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Long getSegmentId() {
        return segmentId;
    }


    public String getActivityNo() {
        return activityNo;
    }

    public void setActivityNo(String activityNo) {
        this.activityNo = activityNo;
    }

    public String getRequestorLogin() {
        return requestorLogin;
    }


    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }
}
