package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;

public class TaskComment extends BaseComment {

    @Nonnull
    @JsonProperty("TaskId")
    @JsonView(CreateUpdate.class)
    private long taskId;


    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }


}
