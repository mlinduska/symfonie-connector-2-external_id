package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;

public class JobComment extends BaseComment {

    @Nonnull
    @JsonProperty("JobId")
    @JsonView(CreateUpdate.class)
    private long jobId;


    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }


}
