package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;

import static com.moravia.symfonie.domain.odata.Views.Read;


public class Language {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(Read.class)
    private String name;

    @JsonProperty("Ietf")
    @JsonView(Read.class)
    private String ietf;

    @JsonProperty("NavisionCode")
    @JsonView(Read.class)
    private String navisionCode;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIetf() {
        return ietf;
    }

    public String getNavisionCode() {
        return navisionCode;
    }

    @Override
    public String toString() {
        return getIetf();
    }
}
