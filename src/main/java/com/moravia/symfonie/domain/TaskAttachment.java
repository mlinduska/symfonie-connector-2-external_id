package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

import static com.moravia.symfonie.domain.odata.Views.Read;

public class TaskAttachment extends BaseAttachment {
    @JsonProperty("TaskId")
    @JsonView(Read.class)
    private Long taskId;

    public Long getTaskId() {
        return taskId;
    }
}
