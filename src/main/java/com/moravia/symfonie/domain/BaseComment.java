package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import org.joda.time.DateTime;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;

public abstract class BaseComment {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @JsonProperty("CompanyCode")
    @JsonView(CreateUpdate.class)
    private String companyCode;

    @Nonnull
    @JsonProperty("CreatedBy")
    @JsonView(CreateUpdate.class)
    private long createdBy;

    @JsonProperty("CreatorLogin")
    @JsonView(Read.class)
    private String creatorLogin;

    @JsonProperty("CreatorEmail")
    @JsonView(Read.class)
    private String creatorEmail;

    @JsonProperty("CreatorName")
    @JsonView(Read.class)
    private String creatorName;

    @JsonProperty("Message")
    @JsonView(CreateUpdate.class)
    private String message;

    @Nonnull
    @JsonProperty("UpdatedAt")
    @JsonView(Read.class)
    private DateTime updatedAt;

    public long getId() {
        return id;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatorLogin() {
        return creatorLogin;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }
}
