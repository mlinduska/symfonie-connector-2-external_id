package com.moravia.symfonie.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;

import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;


public class Project {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Version")
    @JsonView(Read.class)
    private long version;

    @JsonProperty("Name")
    @JsonView(CreateUpdate.class)
    private String name;

    @JsonProperty("Code")
    @JsonView(CreateUpdate.class)
    private String code;

    @Nonnull
    @JsonProperty("SubscribedUsers")
    @JsonView(CreateUpdate.class)
    private List<Long> subscribedUserIds;

    @JsonProperty("Languages")
    @JsonView(CreateUpdate.class)
    private List<String> languages;

    @Nonnull
    @JsonProperty("ProjectState")
    @JsonView(CreateUpdate.class)
    private ProjectStatus projectState;

    @JsonProperty("Notes")
    @JsonView(CreateUpdate.class)
    private String notes;

    public static enum ProjectStatus implements ODataEnum {
        Planning(0),
        Quote(1),
        Order(2),
        Completed(3);

        private int code;

        ProjectStatus(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.ProjectStatus";
        }

        public int getCode() {
            return code;
        }
    }

    public long getId() {
        return id;
    }


    public long getVersion() {
        return version;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Long> getSubscribedUserIds() {
        return subscribedUserIds;
    }

    public void setSubscribedUserIds(List<Long> subscribedUserIds) {
        this.subscribedUserIds = subscribedUserIds;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public ProjectStatus getProjectState() {
        return projectState;
    }

    public void setProjectState(ProjectStatus projectState) {
        this.projectState = projectState;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
