package com.moravia.symfonie.domain;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.Lists;
import com.moravia.symfonie.domain.odata.Expandable;
import com.moravia.symfonie.domain.odata.Nonnull;
import com.moravia.symfonie.domain.odata.ODataEnum;
import org.joda.time.DateTime;

import java.util.List;

import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.domain.odata.Views.Read;

public class Task {
    @Nonnull
    @JsonProperty("Id")
    @JsonView(Read.class)
    private long id;

    @Nonnull
    @JsonProperty("Name")
    @JsonView(CreateUpdate.class)
    private String name;

    @Nonnull
    @JsonProperty("Version")
    @JsonView(Read.class)
    private long version;

    @Nonnull
    @JsonProperty("RequestorId")
    @JsonView(CreateUpdate.class)
    private long requestorId;

    @Nonnull
    @JsonProperty("CreatedAt")
    @JsonView(Read.class)
    private DateTime createdAt;

    @Nonnull
    @JsonProperty("UpdatedAt")
    @JsonView(Read.class)
    private DateTime updatedAt;

    @JsonProperty("WorkflowId")
    @JsonView(CreateUpdate.class)
    private Long workflowId;

    @JsonProperty("JobId")
    @JsonView(CreateUpdate.class)
    private Long jobId;

    @JsonProperty("StartDate")
    @JsonView(CreateUpdate.class)
    private DateTime startDate;

    @JsonProperty("DueDate")
    @JsonView(CreateUpdate.class)
    private DateTime dueDate;

    @JsonProperty("Description")
    @JsonView(CreateUpdate.class)
    private String description;

    @JsonProperty("AcceptedDate")
    @JsonView(Read.class)
    private DateTime acceptedDate;

    @JsonProperty("Assignees")
    @JsonView(CreateUpdate.class)
    private List<Assignee> assignees = Lists.newArrayList();

    @JsonProperty("ServiceTag")
    @JsonView(CreateUpdate.class)
    private String serviceTag;

    @Nonnull
    @JsonProperty("State")
    @JsonView(Read.class)
    private State state;

    @JsonProperty("Tags")
    @JsonView(CreateUpdate.class)
    private List<String> tags;

    @JsonProperty("OrderDate")
    @JsonView(Read.class)
    private DateTime orderDate;

    @Nonnull
    @JsonProperty("IsArchived")
    @JsonView(CreateUpdate.class)
    private boolean archived;

    @Nonnull
    @JsonProperty("Type")
    @JsonView(CreateUpdate.class)
    private Type type;

    @JsonProperty("SourceLanguageCode")
    @JsonView(Read.class)
    private String sourceLanguageCode;

    @JsonProperty("TargetLanguageCode")
    @JsonView(Read.class)
    private String targetLanguageCode;

    @JsonProperty("CustomFields")
    @JsonView(CreateUpdate.class)
    private List<CustomField> customFields = Lists.newArrayList();

    @Expandable
    @JsonProperty("Project")
    @JsonView(Read.class)
    private Project project;

    @Expandable
    @JsonProperty("Attachments")
    @JsonView(Read.class)
    private List<TaskAttachment> attachments;

    @Expandable
    @JsonProperty("FinanceRows")
    @JsonView(Read.class)
    private List<FinanceRow> financeRows;

    public static enum State implements ODataEnum {
        Draft(1),
        HeadsUp(2),
        Order(3),
        InProgress(4),
        Completed(5),
        Canceled(6),
        Rejected(7),
        Approved(8),
        HeadsUpClaimed(9),
        OnHold(10),;

        private int code;

        State(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.TaskStates";
        }

        public int getCode() {
            return code;
        }
    }

    public static enum Type implements ODataEnum {
        Custom(0),
        Translation(1),
        Review(2),
        Arbitration(3),
        Implementation(4),
        TranslatorFeedback(5),
        ReviewerFeedback(6),;

        private int code;

        Type(int code) {
            this.code = code;
        }

        @Override
        public String getJsonValue() {
            return name();
        }

        @Override
        public String getODataPrefix() {
            return "Moravia.Symfonie.Data.TaskType";
        }

        public int getCode() {
            return code;
        }
    }

    public static class CustomField {
        @Nonnull
        @JsonProperty("CustomFieldId")
        @JsonView(Read.class)
        private long id;

        @Nonnull
        @JsonProperty("Name")
        @JsonView(CreateUpdate.class)
        private String name;

        @Nonnull
        @JsonProperty("UpdatedAt")
        @JsonView(Read.class)
        private DateTime updatedAt;

        @JsonProperty("Value")
        @JsonView(CreateUpdate.class)
        private String value;

        @JsonProperty("DefinitionAdditionalData")
        @JsonView(Read.class)
        private String definitionAdditionalData;

        @Nonnull
        @JsonProperty("DefinitionFormatter")
        @JsonView(Read.class)
        private String definitionFormatter;

        @JsonProperty("DefinitionKey")
        @JsonView(Read.class)
        private String definitionKey;

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public DateTime getUpdatedAt() {
            return updatedAt;
        }

        public String getValue() {
            return value;
        }

        public String getDefinitionAdditionalData() {
            return definitionAdditionalData;
        }

        public String getDefinitionFormatter() {
            return definitionFormatter;
        }

        public String getDefinitionKey() {
            return definitionKey;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getVersion() {
        return version;
    }

    public long getRequestorId() {
        return requestorId;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    public Long getWorkflowId() {
        return workflowId;
    }

    public Long getJobId() {
        return jobId;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getDueDate() {
        return dueDate;
    }

    public String getDescription() {
        return description;
    }

    public DateTime getAcceptedDate() {
        return acceptedDate;
    }

    public List<Assignee> getAssignees() {
        return assignees;
    }

    public String getServiceTag() {
        return serviceTag;
    }

    public State getState() {
        return state;
    }

    public List<String> getTags() {
        return tags;
    }

    public DateTime getOrderDate() {
        return orderDate;
    }

    public boolean isArchived() {
        return archived;
    }

    public Type getType() {
        return type;
    }

    public String getSourceLanguageCode() {
        return sourceLanguageCode;
    }

    public String getTargetLanguageCode() {
        return targetLanguageCode;
    }

    public List<CustomField> getCustomFields() {
        return customFields;
    }

    public Project getProject() {
        return project;
    }

    public List<TaskAttachment> getAttachments() {
        return attachments;
    }

    public List<FinanceRow> getFinanceRows() {
        return financeRows;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setRequestorId(long requestorId) {
        this.requestorId = requestorId;
    }


    public void setWorkflowId(Long workflowId) {
        this.workflowId = workflowId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public void setDueDate(DateTime dueDate) {
        this.dueDate = dueDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void setAssignees(List<Assignee> assignees) {
        this.assignees = assignees;
    }


    public void setTags(List<String> tags) {
        this.tags = tags;
    }


    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setServiceTag(String serviceTag) {
        this.serviceTag = serviceTag;
    }

    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }
}
