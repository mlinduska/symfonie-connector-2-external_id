package com.moravia.symfonie;

public class SymfonieException extends RuntimeException {
    public SymfonieException(String message) {
        super(message);
    }

    public SymfonieException(String message, Throwable cause) {
        super(message, cause);
    }

    public SymfonieException(Throwable cause) {
        super(cause);
    }
}
