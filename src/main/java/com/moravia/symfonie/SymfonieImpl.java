package com.moravia.symfonie;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.io.ByteStreams;
import com.moravia.symfonie.domain.*;
import com.moravia.symfonie.domain.odata.FunctionResultWrapper;
import com.moravia.symfonie.domain.odata.PageableListResponse;
import com.moravia.symfonie.query.Query;
import com.moravia.symfonie.util.json.JsonUtils;
import com.moravia.symfonie.util.rest.RestException;
import com.moravia.symfonie.util.rest.RestTemplate;
import com.moravia.symfonie.util.rest.UnexpectedOperationResult;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.moravia.symfonie.domain.BaseAttachment.Type;
import static com.moravia.symfonie.domain.odata.Views.CreateUpdate;
import static com.moravia.symfonie.filter.Filters.eq;
import static com.moravia.symfonie.query.QueryBuilder.emptyQuery;
import static com.moravia.symfonie.query.QueryBuilder.queryBuilder;
import static java.util.Collections.singletonMap;

public class SymfonieImpl implements Symfonie {
    private static final String USERS = "/Users";
    private static final String LANGUAGES = "/Languages";
    private static final String TAGS = "/Tags";
    private static final String PROJECTS = "/Projects";
    private static final String PROJECT_COMPANIES = "/ProjectCompanies";
    private static final String JOBS = "/Jobs";
    private static final String WORKFLOWS = "/Workflows";
    private static final String TASKS = "/Tasks";
    private static final String JOB_COMMENTS = "/JobComments";
    private static final String TASK_COMMENTS = "/Comments";
    private static final String FINANCE_ROWS = "/TaskAmounts";
    private static final String JOB_ATTACHMENTS = "/JobAttachments";
    private static final String TASK_ATTACHMENTS = "/TaskAttachments";

    private static final String UPLOAD_ATTACHMENT_JSON_PART = "json";
    private static final String UPLOAD_ATTACHMENT_FILE_PART = "file";

    private String apiUrl;
    private String apiToken;

    private CloseableHttpClient httpClient;
    private ObjectMapper jsonMapper;
    private RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        checkNotNull(apiUrl);
        checkNotNull(apiToken);
        httpClient = createHttpClient();
        jsonMapper = createJsonMapper();
        restTemplate = new RestTemplate(httpClient, jsonMapper);
    }

    private CloseableHttpClient createHttpClient() {
        return HttpClientBuilder.create()
                .addInterceptorFirst((HttpRequest request, HttpContext context) -> request.addHeader("Authorization", "authToken " + apiToken))
                .build();
    }

    private ObjectMapper createJsonMapper() {
        return JsonUtils.symfonieMapper();
    }

    @PreDestroy
    public void destroy() throws IOException {
        httpClient.close();
    }


    @Override
    public User getUser(long id) {
        URI uri = RequestBuilder.get()
                .setUri(apiUrl + USERS + "(" + id + ")")
                .addParameter("$expand", "Vacations").build().getURI();
        return restTemplate.getForObject(uri, User.class);
    }

    @Override
    public List<User> findUsers(Query query) {
        return findObjects(USERS, query, new TypeReference<PageableListResponse<User>>() {
        });
    }

    @Override
    public User getMe() {
        return restTemplate.getForObject(apiUrl + USERS + "/Default.WhoAmI()", User.class);
    }


    @Override
    public Language getLanguage(long id) {
        return restTemplate.getForObject(apiUrl + LANGUAGES + "(" + id + ")", Language.class);
    }

    @Override
    public List<Language> getAllLanguages() {
        return findLanguages(emptyQuery());
    }

    @Override
    public List<Language> findLanguages(Query query) {
        return findObjects(LANGUAGES, query, new TypeReference<PageableListResponse<Language>>() {
        });
    }


    @Override
    public Tag getTag(long id) {
        return restTemplate.getForObject(apiUrl + TAGS + "(" + id + ")", Tag.class);
    }

    @Override
    public List<Tag> getAllTags() {
        return findTags(emptyQuery());
    }

    @Override
    public List<Tag> findTags(Query query) {
        return findObjects(TAGS, query, new TypeReference<PageableListResponse<Tag>>() {
        });
    }


    @Override
    public Project getProject(long id) {
        return restTemplate.getForObject(apiUrl + PROJECTS + "(" + id + ")", Project.class);
    }

    @Override
    public List<Project> findProjects(Query query) {
        return findObjects(PROJECTS, query, new TypeReference<PageableListResponse<Project>>() {
        });
    }

    @Override
    public List<Project> getUserProjects(Long userId) {
        return findObjects(PROJECTS + "/Default.GetUserProjects(userId=" + userId + ")", emptyQuery(), new TypeReference<PageableListResponse<Project>>() {
        });
    }


    @Override
    public ProjectCompany getProjectCompany(long id) {
        return restTemplate.getForObject(apiUrl + PROJECT_COMPANIES + "(" + id + ")", ProjectCompany.class);
    }

    @Override
    public List<ProjectCompany> getProjectCompanies(long projectId) {
        return findProjectCompanies(queryBuilder().filter(eq("ProjectId", projectId)).build());
    }

    @Override
    public List<ProjectCompany> findProjectCompanies(Query query) {
        return findObjects(PROJECT_COMPANIES, query, new TypeReference<PageableListResponse<ProjectCompany>>() {
        });
    }


    @Override
    public Job getJob(long id) {
        URI uri = RequestBuilder.get()
                .setUri(apiUrl + JOBS + "(" + id + ")")
                .addParameter("$expand", "Workflows")
                .build().getURI();
        return restTemplate.getForObject(uri, Job.class);
    }

    @Override
    public List<Job> findJobs(Query query) {
        return findObjects(JOBS, query, new TypeReference<PageableListResponse<Job>>() {
        });
    }

    @Override
    public Job createJob(Job job) {
        return restTemplate.postForObject(job, apiUrl + JOBS, Job.class, CreateUpdate.class);
    }

    @Override
    public Job updateJob(Job update) {
        String uri = apiUrl + JOBS + "(" + update.getId() + ")";
        restTemplate.patch(update, uri, Job.class, CreateUpdate.class);
        return getJob(update.getId());
    }

    @Override
    public Job doJobAction(long jobId, JobAction action) {
        String jobUri = apiUrl + JOBS + "(" + jobId + ")";
        callODataAction(jobUri + "/Default.ExecuteJobCommand", singletonMap("jobCommand", action));
        return restTemplate.getForObject(jobUri, Job.class);
    }


    @Override
    public Workflow getWorkflow(long id) {
        return restTemplate.getForObject(apiUrl + WORKFLOWS + "(" + id + ")", Workflow.class);
    }

    @Override
    public List<Workflow> getJobWorkflows(long jobId) {
        Query query = queryBuilder().filter(eq("JobId", jobId)).build();
        return findWorkflows(query);
    }

    @Override
    public List<Workflow> findWorkflows(Query query) {
        return findObjects(WORKFLOWS, query, new TypeReference<PageableListResponse<Workflow>>() {
        });
    }

    @Override
    public Workflow createWorkflow(Workflow workflow) {
        return restTemplate.postForObject(workflow, apiUrl + WORKFLOWS, Workflow.class, CreateUpdate.class);
    }

    @Override
    public Workflow updateWorkflow(Workflow update) {
        restTemplate.patch(update, apiUrl + WORKFLOWS + "(" + update.getId() + ")", Workflow.class, CreateUpdate.class);
        return getWorkflow(update.getId());
    }


    @Override
    public Task getTask(long id) {
        URI uri = RequestBuilder.get().setUri(apiUrl + TASKS + "(" + id + ")")
                .addParameter("$expand", "Project,Attachments,FinanceRows")
                .build().getURI();
        return restTemplate.getForObject(uri, Task.class);
    }

    @Override
    public List<Task> findTasks(Query query) {
        return findObjects(TASKS, query, new TypeReference<PageableListResponse<Task>>() {
        });
    }

    @Override
    public Task createTask(Task task) {
        return restTemplate.postForObject(task, apiUrl + TASKS, Task.class, CreateUpdate.class);
    }

    @Override
    public Task updateTask(Task update) {
        restTemplate.patch(update, apiUrl + TASKS + "(" + update.getId() + ")", Task.class, CreateUpdate.class);
        return getTask(update.getId());
    }

    @Override
    public Task doTaskAction(long taskId, TaskAction action) {
        String taskUri = apiUrl + TASKS + "(" + taskId + ")";
        callODataAction(taskUri + "/Default.ExecuteTaskCommand", singletonMap("taskCommand", action));
        return restTemplate.getForObject(taskUri, Task.class);
    }


    @Override
    public JobComment getJobComment(long id) {
        return restTemplate.getForObject(apiUrl + JOB_COMMENTS + "(" + id + ")", JobComment.class);
    }

    @Override
    public List<JobComment> getJobComments(long jobId) {
        Query query = queryBuilder().filter(eq("JobId", jobId)).build();
        return findJobComments(query);
    }

    @Override
    public List<JobComment> findJobComments(Query query) {
        return findObjects(JOB_COMMENTS, query, new TypeReference<PageableListResponse<JobComment>>() {
        });
    }

    @Override
    public JobComment createJobComment(JobComment comment) {
        return restTemplate.postForObject(comment, apiUrl + JOB_COMMENTS, JobComment.class, CreateUpdate.class);
    }


    @Override
    public TaskComment getTaskComment(long id) {
        return restTemplate.getForObject(apiUrl + TASK_COMMENTS + "(" + id + ")", TaskComment.class);
    }

    @Override
    public List<TaskComment> getTaskComments(long taskId) {
        Query query = queryBuilder().filter(eq("TaskId", taskId)).build();
        return findTaskComments(query);
    }

    @Override
    public List<TaskComment> findTaskComments(Query query) {
        return findObjects(TASK_COMMENTS, query, new TypeReference<PageableListResponse<TaskComment>>() {
        });
    }

    @Override
    public TaskComment createTaskComment(TaskComment comment) {
        return restTemplate.postForObject(comment, apiUrl + TASK_COMMENTS, TaskComment.class, CreateUpdate.class);
    }


    @Override
    public JobAttachment getJobAttachment(long id) {
        return restTemplate.getForObject(apiUrl + JOB_ATTACHMENTS + "(" + id + ")", JobAttachment.class);
    }

    @Override
    public TaskAttachment getTaskAttachment(long id) {
        return restTemplate.getForObject(apiUrl + TASK_ATTACHMENTS + "(" + id + ")", TaskAttachment.class);
    }

    @Override
    public List<JobAttachment> getJobAttachments(long jobId) {
        return findJobAttachments(queryBuilder().filter(eq("JobId", jobId)).build());
    }

    @Override
    public List<TaskAttachment> getTaskAttachments(long taskId) {
        return findTaskAttachments(queryBuilder().filter(eq("TaskId", taskId)).build());
    }

    @Override
    public List<JobAttachment> findJobAttachments(Query query) {
        return findObjects(JOB_ATTACHMENTS, query, new TypeReference<PageableListResponse<JobAttachment>>() {
        });
    }

    @Override
    public List<TaskAttachment> findTaskAttachments(Query query) {
        return findObjects(TASK_ATTACHMENTS, query, new TypeReference<PageableListResponse<TaskAttachment>>() {
        });

    }

    @Override
    public JobAttachment uploadJobAttachment(long jobId, File file, Type type, String name) {
        return uploadJobAttachment(jobId, file, type, name, null);
    }

    @Override
    public JobAttachment uploadJobAttachment(long jobId, File file, Type type, String name, String targetLanguageCode) {
        Map<String, Object> params = Maps.newLinkedHashMap();
        params.put("JobId", jobId);
        params.put("TargetLanguageCode", targetLanguageCode);
        return uploadAttachmentInternal(JOB_ATTACHMENTS, file, type, name, params, JobAttachment.class);
    }

    @Override
    public TaskAttachment uploadTaskAttachment(long taskId, File file, Type type, String name) {
        Map<String, Object> params = Collections.singletonMap("TaskId", taskId);
        return uploadAttachmentInternal(TASK_ATTACHMENTS, file, type, name, params, TaskAttachment.class);
    }

    private <T extends BaseAttachment> T uploadAttachmentInternal(String endpoint, File file, Type type, String name, Map<String, Object> additionalParams, Class<T> resultType) {
        try {
            Map<String, Object> metadata = Maps.newLinkedHashMap(additionalParams);
            metadata.put("Name", name);
            metadata.put("FileType", type.getJsonValue());

            HttpEntity entity = MultipartEntityBuilder.create()
                    .addTextBody(UPLOAD_ATTACHMENT_JSON_PART, jsonMapper.writeValueAsString(metadata), ContentType.APPLICATION_JSON)
                    .addBinaryBody(UPLOAD_ATTACHMENT_FILE_PART, file, ContentType.APPLICATION_OCTET_STREAM, name)
                    .setMode(HttpMultipartMode.RFC6532)
                    .build();

            HttpUriRequest request = RequestBuilder.post().setUri(apiUrl + endpoint).setEntity(entity).build();
            return httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_CREATED) {
                    return jsonMapper.reader().withType(resultType).readValue(response.getEntity().getContent());
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new SymfonieException(e);
        }
    }

    @Override
    public File downloadJobAttachment(JobAttachment attachment, File dir, String name) {
        return downloadAttachmentInternal(attachment, dir, name);
    }

    @Override
    public File downloadTaskAttachment(TaskAttachment attachment, File dir, String name) {
        return downloadAttachmentInternal(attachment, dir, name);
    }

    private File downloadAttachmentInternal(BaseAttachment attachment, File dir, String name) {
        try {
            HttpUriRequest request = RequestBuilder.get().setUri(apiUrl + "/" + attachment.getDownloadUrl()).build();
            return httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    File file = new File(dir, name);
                    try (OutputStream out = new BufferedOutputStream(new FileOutputStream(file))) {
                        ByteStreams.copy(response.getEntity().getContent(), out);
                    }
                    return file;
                } else if (statusCode == HttpStatus.SC_NOT_FOUND) {
                    return null;
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new SymfonieException(e);
        }
    }

    @Override
    public void deleteJobAttachment(long id) {
        restTemplate.delete(apiUrl + JOB_ATTACHMENTS + "(" + id + ")");
    }

    @Override
    public void deleteTaskAttachment(long id) {
        restTemplate.delete(apiUrl + TASK_ATTACHMENTS + "(" + id + ")");
    }


    @Override
    public FinanceRow getFinanceRow(long id) {
        URI uri = RequestBuilder.get().setUri(apiUrl + FINANCE_ROWS + "(" + id + ")")
                .addParameter("$expand", "Prices").build()
                .getURI();
        return restTemplate.getForObject(uri, FinanceRow.class);
    }

    @Override
    public List<FinanceRow> getFinanceRows(long taskId) {
        Query query = queryBuilder()
                .filter(eq("TaskId", taskId))
                .expand("Prices")
                .build();
        return findFinanceRows(query);
    }

    @Override
    public List<FinanceRow> findFinanceRows(Query query) {
        return findObjects(FINANCE_ROWS, query, new TypeReference<PageableListResponse<FinanceRow>>() {
        });
    }

    @Override
    public FinanceRow createFinanceRow(FinanceRow row) {
        return restTemplate.postForObject(row, apiUrl + FINANCE_ROWS, FinanceRow.class, CreateUpdate.class);
    }

    @Override
    public FinanceRow updateFinanceRow(FinanceRow update) {
        restTemplate.patch(update, apiUrl + FINANCE_ROWS + "(" + update.getId() + ")", FinanceRow.class, CreateUpdate.class);
        return getFinanceRow(update.getId());
    }

    @Override
    public void deleteFinanceRow(long id) {
        restTemplate.delete(apiUrl + FINANCE_ROWS + "(" + id + ")");
    }


    @Override
    public List<User> getProjectVendors(long projectId, String language, int companyPos, boolean includeLowerPos, String tag) {
        URI uri = RequestBuilder.get().setUri(apiUrl + USERS + "/Default.FindProjectVendors(" +
                "projectId=" + projectId + "," +
                "targetLanguageCode='" + language + "'," +
                "preferenceLevel=" + companyPos + "," +
                "tagName=" + (tag == null ? "null" : "'" + tag + "'") + "," +
                "incLowerPrefs=" + includeLowerPos +
                ")").build().getURI();
        return restTemplate.getForObject(uri, new TypeReference<FunctionResultWrapper<List<User>>>() {
        }).getValue();
    }


    // utility methods
    private <T> List<T> findObjects(String endpoint, Query query, TypeReference<PageableListResponse<T>> holder) {
        List<T> result = Lists.newArrayList();

        URI uri = RequestBuilder.get().setUri(apiUrl + endpoint).addParameters(query.getUriParams()).build().getURI();
        do {
            PageableListResponse<T> response = restTemplate.getForObject(uri, holder);
            result.addAll(response.getValues());
            uri = response.getNextPageLink();
        } while (uri != null);

        return result;
    }

    private void callODataAction(String actionUri, Map<String, Object> params) {
        try {
            String json = jsonMapper.writeValueAsString(params);
            HttpUriRequest request = RequestBuilder.post().setUri(actionUri)
                    .setEntity(new StringEntity(json, ContentType.APPLICATION_JSON))
                    .build();
            httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    return null;
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new RestException(e);
        }
    }

    // IoC
    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }
}
