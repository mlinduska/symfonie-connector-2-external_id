package com.moravia.symfonie.util.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.google.common.base.Splitter;
import org.joda.time.DateTimeZone;

import java.io.IOException;
import java.util.List;


public class UserTimezoneDeserializer extends JsonDeserializer<DateTimeZone> {
    @Override
    public DateTimeZone deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        List<String> parts = Splitter.on(":").trimResults().splitToList(jp.getText());
        Integer hours = Integer.valueOf(parts.get(0));
        Integer minutes = Integer.valueOf(parts.get(1));
        return DateTimeZone.forOffsetHoursMinutes(hours, minutes);
    }
}
