package com.moravia.symfonie.util.json;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.joda.time.DateTime;

import java.net.URI;

public class SymfonieJsonModule extends SimpleModule {

    public SymfonieJsonModule() {
        super("Symfonie2");

        addSerializer(DateTime.class, new DateTimeSerializer());
        addDeserializer(DateTime.class, new DateTimeDeserializer());
        addDeserializer(URI.class, new UriDeserializer());
    }

}
