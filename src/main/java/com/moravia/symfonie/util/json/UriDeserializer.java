package com.moravia.symfonie.util.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


public class UriDeserializer extends JsonDeserializer<URI> {
    @Override
    public URI deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        String text = jp.getText();
        try {
            return new URI(text);
        } catch (URISyntaxException e) {
            throw new InvalidFormatException("Can't deserialize '" + text + "' to URI", text, URI.class);
        }
    }
}
