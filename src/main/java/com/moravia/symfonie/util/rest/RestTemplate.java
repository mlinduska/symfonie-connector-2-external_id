package com.moravia.symfonie.util.rest;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;

public class RestTemplate {
    private static final Logger log = LoggerFactory.getLogger(RestTemplate.class);
    private static final String PATCH = "PATCH";

    private HttpClient httpClient;
    private ObjectMapper jsonMapper;

    public RestTemplate(HttpClient httpClient, ObjectMapper jsonMapper) {
        this.httpClient = httpClient;
        this.jsonMapper = jsonMapper;
    }

    // GET
    public <T> T getForObject(String uri, Class<? extends T> type) {
        return getForObject(URI.create(uri), type);
    }

    public <T> T getForObject(URI uri, Class<? extends T> type) {
        return getForObjectInternal(uri, jsonMapper.reader().withType(type));
    }

    public <T> T getForObject(String uri, TypeReference<? extends T> type) {
        return getForObject(URI.create(uri), type);
    }

    public <T> T getForObject(URI uri, TypeReference<? extends T> type) {
        return getForObjectInternal(uri, jsonMapper.reader().withType(type));
    }

    private <T> T getForObjectInternal(URI uri, ObjectReader reader) {
        try {
            HttpUriRequest request = RequestBuilder.get().setUri(uri).build();
            if (log.isDebugEnabled()) log.debug("request {}", request);
            return httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    String json = EntityUtils.toString(response.getEntity());
                    if (log.isDebugEnabled()) log.debug("response {}: \n{}", request, json);
                    return reader.readValue(json);
                } else if (statusCode == HttpStatus.SC_NOT_FOUND) {
                    return null;
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new RestException(e);
        }
    }

    // POST
    public <T> T postForObject(T object, String uri, Class<? extends T> type) {
        return postForObject(object, URI.create(uri), type);
    }

    public <T> T postForObject(T object, URI uri, Class<? extends T> type) {
        return postForObjectInternal(object, uri, jsonMapper.writer().withType(type), jsonMapper.reader().withType(type));
    }

    public <T> T postForObject(T object, String uri, Class<? extends T> type, Class<?> view) {
        return postForObject(object, URI.create(uri), type, view);
    }

    public <T> T postForObject(T object, URI uri, Class<? extends T> type, Class<?> view) {
        return postForObjectInternal(object, uri, jsonMapper.writer().withView(view).withType(type), jsonMapper.reader().withType(type));
    }

    private <T> T postForObjectInternal(T object, URI uri, ObjectWriter writer, ObjectReader reader) {
        try {
            String json = writer.writeValueAsString(object);
            HttpUriRequest request = RequestBuilder.post().setUri(uri)
                    .setEntity(new StringEntity(json, ContentType.APPLICATION_JSON))
                    .build();
            if (log.isDebugEnabled()) log.debug("request {}: \n{}", request, json);
            return httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_CREATED) {
                    String jsonRes = EntityUtils.toString(response.getEntity());
                    if (log.isDebugEnabled()) log.debug("response {}: \n{}", request, jsonRes);
                    return reader.readValue(jsonRes);
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new RestException(e);
        }
    }

    // PATCH

    public <T> void patch(T object, String uri, Class<? extends T> type) {
        patch(object, URI.create(uri), type);
    }

    public <T> void patch(T object, URI uri, Class<? extends T> type) {
        patchInternal(object, uri, jsonMapper.writer().withType(type));
    }

    public <T> void patch(T object, String uri, Class<? extends T> type, Class<?> view) {
        patch(object, URI.create(uri), type, view);
    }

    public <T> void patch(T object, URI uri, Class<? extends T> type, Class<?> view) {
        patchInternal(object, uri, jsonMapper.writer().withView(view).withType(type));
    }

    private void patchInternal(Object object, URI uri, ObjectWriter writer) {
        try {
            String json = writer.writeValueAsString(object);
            HttpUriRequest request = RequestBuilder.create(PATCH).setUri(uri)
                    .setEntity(new StringEntity(json, ContentType.APPLICATION_JSON))
                    .build();
            if (log.isDebugEnabled()) log.debug("request {}: \n{}", request, json);
            httpClient.execute(request, response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK || statusCode == HttpStatus.SC_NO_CONTENT) {
                    return null;
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new RestException(e);
        }
    }

    // DELETE

    public void delete(String uri) {
        deleteInternal(URI.create(uri));
    }

    public void delete(URI uri) {
        deleteInternal(uri);
    }

    private void deleteInternal(URI uri) {
        try {
            httpClient.execute(RequestBuilder.delete().setUri(uri).build(), response -> {
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == HttpStatus.SC_OK) {
                    return null;
                } else {
                    throw new UnexpectedOperationResult(statusCode, EntityUtils.toString(response.getEntity()));
                }
            });
        } catch (IOException e) {
            throw new RestException(e);
        }
    }


}
