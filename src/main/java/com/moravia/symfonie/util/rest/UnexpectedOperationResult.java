package com.moravia.symfonie.util.rest;


public class UnexpectedOperationResult extends RestException {
    private int statusCode;
    private String message;

    public UnexpectedOperationResult(int statusCode) {
        super(String.format("Unexpected result: code %d", statusCode));
        this.statusCode = statusCode;
    }

    public UnexpectedOperationResult(int statusCode, String message) {
        super(String.format("Unexpected result: code %d, message %s", statusCode, message));
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

}
