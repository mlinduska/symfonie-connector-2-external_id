package com.moravia.symfonie;

import com.moravia.symfonie.domain.*;
import com.moravia.symfonie.query.Query;

import java.io.File;
import java.util.List;

import static com.moravia.symfonie.domain.BaseAttachment.Type;

public interface Symfonie {
    User getUser(long id);

    User getMe();

    List<User> findUsers(Query query);


    Language getLanguage(long id);

    List<Language> getAllLanguages();

    List<Language> findLanguages(Query query);


    Tag getTag(long id);

    List<Tag> getAllTags();

    List<Tag> findTags(Query query);


    Project getProject(long id);

    List<Project> getUserProjects(Long userId);

    List<Project> findProjects(Query query);


    ProjectCompany getProjectCompany(long id);

    List<ProjectCompany> getProjectCompanies(long projectId);

    List<ProjectCompany> findProjectCompanies(Query query);


    Job getJob(long id);

    List<Job> findJobs(Query query);

    Job createJob(Job job);

    Job updateJob(Job update);

    Job doJobAction(long jobId, JobAction action);


    Workflow getWorkflow(long id);

    List<Workflow> getJobWorkflows(long jobId);

    List<Workflow> findWorkflows(Query query);

    Workflow createWorkflow(Workflow workflow);

    Workflow updateWorkflow(Workflow update);


    Task getTask(long id);

    List<Task> findTasks(Query query);

    Task createTask(Task task);

    Task updateTask(Task update);

    Task doTaskAction(long taskId, TaskAction action);


    JobComment getJobComment(long id);

    List<JobComment> getJobComments(long jobId);

    List<JobComment> findJobComments(Query query);

    JobComment createJobComment(JobComment comment);


    TaskComment getTaskComment(long id);

    List<TaskComment> getTaskComments(long taskId);

    List<TaskComment> findTaskComments(Query query);

    TaskComment createTaskComment(TaskComment comment);


    JobAttachment getJobAttachment(long id);

    List<JobAttachment> getJobAttachments(long jobId);

    List<JobAttachment> findJobAttachments(Query query);

    JobAttachment uploadJobAttachment(long jobId, File file, Type type, String name);

    JobAttachment uploadJobAttachment(long jobId, File file, Type type, String name, String targetLanguageCode);

    File downloadJobAttachment(JobAttachment attachment, File dir, String name);

    void deleteJobAttachment(long id);


    TaskAttachment getTaskAttachment(long id);

    List<TaskAttachment> getTaskAttachments(long taskId);

    List<TaskAttachment> findTaskAttachments(Query query);

    TaskAttachment uploadTaskAttachment(long taskId, File file, Type type, String name);

    File downloadTaskAttachment(TaskAttachment attachment, File dir, String name);

    void deleteTaskAttachment(long id);


    FinanceRow getFinanceRow(long id);

    List<FinanceRow> getFinanceRows(long taskId);

    List<FinanceRow> findFinanceRows(Query query);

    FinanceRow createFinanceRow(FinanceRow row);

    FinanceRow updateFinanceRow(FinanceRow update);

    void deleteFinanceRow(long id);


    List<User> getProjectVendors(long projectId, String language, int companyPos, boolean includeLowerPos, String tag);


}
